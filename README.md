# Algorand Smart Contract Model

- Account
  - [Delete](./account/delete.md#delete_account)
  - [Recover](./account/recover.md#recover_account)
  - [Generate random (malformed) address](./account/create_random_address.md#create_malformed_address)
  - [Client - Close](./account/client_nodejs/close.js#close_account)
  - [Client - Create](./account/client_nodejs/create.js#create_account)
  - [Client - Create wallet](./account/client_nodejs/create_wallet.js#create_wallet)
  - [Client - Info](./account/client_nodejs/info.js#info_account)

- Interaction with a node
  - [Check node status](./node_interaction/check_node_status.md#check_node_status)
  - [Open a shell](./node_interaction/open_shell.md#open_shell)

- Smart contract
  - Stateless
    - [Create a Contract account](./smart_contract/stateless/create_contract_account.md#create_contract_account)
    - [Close a Contract account](./smart_contract/stateless/close_contract_account.md#close_contract_account)
    - [Creating the same Contract account](./smart_contract/stateless/same_contract_account.md#same_script)
    - [Re-create a Contract account](./smart_contract/stateless/re_create_contract_account.md#re_create_contract_account)
    - [Multisig](./smart_contract/stateless/multisig.md#multisig)
  - Stateful
    - [Example stateful contract](./smart_contract/stateful/example_stateful_contract.md)
    - [Local state](./smart_contract/stateful/local_state.md)
    - [Global state](./smart_contract/stateful/global_state.md)
    

- Transactions
  - [Single transaction](./transactions/single_transaction.md#single_transaction)
  - [Single transaction with no sign](./transactions/single_transaction_no_sign.md#send_single_transaction_no_sign)
  - [Client - Send transaction](./transactions/client_nodejs/send.js#send_single_transaction)
  - [Pay from a sender to itself](./transactions/tx-pay-snd_eq_rcv.md#pay-sender-eq-receiver)
  - [Close to another address](./transactions/tx-close.md#close-to-another-address)
  - [Close to the sender](./transactions/tx-close.md#close-to-the-sender)
  - [Asset generation](./transactions/tx-gen-optin-burn.md#gen)
  - [Asset opt-in](./transactions/tx-gen-optin-burn.md#opt-in)
  - [Asset burn (creator without all token units)](./transactions/tx-gen-optin-burn.md#burn-creator-without-all-token-units)
  - [Asset burn (creator is manager)](./transactions/tx-gen-optin-burn.md#burn-creator-is-manager)
  - [Asset burn (creator is not manager)](./transactions/tx-gen-optin-burn.md#burn-creator-is-not-manager)
  - [Asset delegate](./transactions/tx-asset-config.md#delegate)
  - [Send frozen asset](./transactions/tx-freeze.md#send-frozen-asset)
  - [Freeze twice](./transactions/tx-freeze.md#freeze-twice)
  - [Unfreezeing an asset](./transactions/tx-freeze.md#unfreezing-an-asset)
  - [Receive frozen asset](./transactions/tx-freeze.md#receive-frozen-asset)
  - [Freeze non-existing asset](./transactions/tx-freeze.md#freeze-non-existing-asset)
  - [Freeze non-owned asset](./transactions/tx-freeze.md#freeze-non-owned-asset)
  - [Freezing in the freezer manager](./transactions/tx-freeze.md#freezing-in-the-freezer-manager)
  - [Opt-in a frozen asset](./transactions/tx-freeze.md#opt-in-a-frozen-asset)
  - [Sending zero asset units](./transactions/tx-freeze.md#sending-zero-asset-units)

- Labels
  - [Empty label](./labels/empty_label.md#empty_label)

- Use cases
  - Stateless
    - [Oracle](./use_cases/stateless/oracle.md#oracle)
  - Stateful
    - [Permissionless voting](./use_cases/stateful/permissionless_voting.md#permissionless-voting)
    - [Permissioned voting](./use_cases/stateful/permissioned_voting.md#permissioned-voting)
    - [Crowdfunding](./use_cases/stateful/crowdfunding.md#crowdfunding)
    - [Multi-player lottery](./use_cases/stateful/lottery.md#multi-player-lottery)
